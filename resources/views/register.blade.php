@extends('layout.master')

@section('judul')
Buat Account Baru!
@endsection

@section('content')
<form action="/welcome" method="post">
    @csrf
    <label>First name:</label><br><br>
    <input type="text" name="namadepan">
    <br><br>

    <label>Last name:</label><br><br>
    <input type="text" name="namabelakang">
    <br><br>

    <input type="submit" name="signup" value="Sign Up">
</form>
@endsection