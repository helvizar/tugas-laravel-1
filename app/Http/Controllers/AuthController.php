<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function pendaftaran(){
        return view('register');
    }

    public function welcome(Request $request){
        // dd($request->all());

        $namadepan = $request['namadepan'];
        $namabelakang = $request['namabelakang'];

        return view('welcome', compact('namadepan', 'namabelakang'));
    }
}
